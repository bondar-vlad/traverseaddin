// UserPropertyManagerPage.h : Declaration of the CUserPropertyManagerPage

#pragma once
#include "TraverseAddin_i.h"
#include "resource.h"       // main symbols
#include <comsvcs.h>
#include <functional>
#include <initializer_list>
class CTraverseAddin;
class CPMPageHandler;

//Control IDs
#define LIST_PROPERTIES 0
#define COMBOBOX_BODY_TYPE 1
#define NUM_CENTER_X 2
#define NUM_CENTER_Y 3
#define NUM_CENTER_Z 4
#define NUM_AXIS_X 5
#define NUM_AXIS_Y 6
#define NUM_AXIS_Z 7
#define NUM_WIDTH 8
#define NUM_LENGTH 9
#define NUM_HEIGHT 10
#define NUM_RADIUS 11
#define NUM_BASE_RADIUS 12
#define NUM_TOP_RADIUS 13
#define CHECKBOX_BODY_COPY 14
#define COMBOBOX_BODY_COPY 15

#define CURVE_PARAMS_NUM 11
#define CURVE_FIRST_LENGTH_PARAM 6
#define CURVE_SECOND_LENGTH_PARAM 7

#define BOX_ITEM 0
#define CONE_ITEM 2
#define CYLINDER_ITEM 1

// count of each body type dimensions
#define BOX_DIM_ARRAY_SIZE 9
#define CONE_DIM_ARRAY_SIZE 9
#define CYLINDER_DIM_ARRAY_SIZE 8

//defaults
#define DEFAULT_BODY_COPY VARIANT_FALSE
#define DEFAULT_BODY_TYPE BOX_ITEM

// CUserPropertyManagerPage

class ATL_NO_VTABLE CUserPropertyManagerPage :
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CUserPropertyManagerPage, &CLSID_UserPropertyManagerPage>,
	public IDispatchImpl<IUserPropertyManagerPage, &IID_IUserPropertyManagerPage, &LIBID_TraverseAddinLib, /*wMajor =*/ 1, /*wMinor =*/ 0>
{
	friend class CPMPageHandler;
public:

private:
	CTraverseAddin *UserAddIn;
	CComPtr<ISldWorks> ISwApp;
	CComPtr<IPropertyManagerPage2> SwPropertyPage;
	CComObject<CPMPageHandler> *Handler;
	CComPtr<IModeler> Modeler;

	// use CSimpleArray, because we need dynamic array
	// and we can use GetData to access pointer to this array and pass it to API methods
	CSimpleArray<BSTR> BodyArrNames;

	CComQIPtr<IBody2> CurrentCopyBody;
	BSTR CurrentCopyBodyName;

	template<typename Item>
	void ArrayProcess(VARIANT* pvItemArr, void (CUserPropertyManagerPage::*fItem)(CComQIPtr<Item>));
	static HRESULT ArrayCount(SAFEARRAY * pSafeArray, long * nResult);

	// Display Items
	void EdgeDisplay(CComQIPtr<IEdge> pEdge);
	void FaceDisplay(CComQIPtr<IFace> pFace);
	void BodyDisplay(CComQIPtr<IBody2> pBody);
	void BodyNameAdd(CComQIPtr<IBody2> pBody);
	void SetBodyForCopy(CComQIPtr<IBody2> pBody);

	template<typename IControlType>
	struct ControlConfig
	{
		IControlType ** Pointer;
		long Caption;
		long Tip;
		long Id;
		ControlConfig(IControlType ** pointer, const long caption, const long tip, const long id) : Pointer(pointer), Caption(caption), Tip(tip), Id(id) {};
		~ControlConfig() {}
	};

protected:
	//Generic Control
	IPropertyManagerPageControl *Control;

	//Custom controls
	IPropertyManagerPageListbox* ListProperties;
	IPropertyManagerPageCombobox* ComboboxBodyType;

	IPropertyManagerPageNumberbox* NumCenterX;
	IPropertyManagerPageNumberbox* NumCenterY;
	IPropertyManagerPageNumberbox* NumCenterZ;

	IPropertyManagerPageNumberbox* NumAxisX;
	IPropertyManagerPageNumberbox* NumAxisY;
	IPropertyManagerPageNumberbox* NumAxisZ;

	IPropertyManagerPageNumberbox* NumWidth;
	IPropertyManagerPageNumberbox* NumLength;
	IPropertyManagerPageNumberbox* NumHeight;

	IPropertyManagerPageNumberbox* NumRadius;
	IPropertyManagerPageNumberbox* NumBaseRadius;
	IPropertyManagerPageNumberbox* NumTopRadius;

	IPropertyManagerPageCheckbox* CheckboxBodyCopy;

	IPropertyManagerPageCombobox* ComboboxBodyCopy;

	short CurrentSelection;
	// dimensions for creating body
	double BodyDimArray[9];

public:
	CUserPropertyManagerPage()
	{
	}

	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}

	void FinalRelease()
	{
	}

	DECLARE_REGISTRY_RESOURCEID(IDR_USERPROPERTYMANAGERPAGE)

	DECLARE_NOT_AGGREGATABLE(CUserPropertyManagerPage)

	BEGIN_COM_MAP(CUserPropertyManagerPage)
		COM_INTERFACE_ENTRY(IUserPropertyManagerPage)
		COM_INTERFACE_ENTRY(IDispatch)
	END_COM_MAP()




	// IUserPropertyManagerPage
public:
	void Init(CTraverseAddin *app);
	void CreatePropertyManagerPage();
	void Destroy();
	void Show() const;
	void AddControls();
	void DisplayProperties();
	CComPtr<IPropertyManagerPage2> GetPropertyPagePtr() const;
	void AddNumberBoxes(std::initializer_list<CUserPropertyManagerPage::ControlConfig<IPropertyManagerPageNumberbox>>);
	void ActivateNewBodyControls(long activationBodyId);
	void ActivateCopyBodyControls();
	void ApplyControlMethod(std::initializer_list<IDispatch*> controls, void(*method)(IPropertyManagerPageControl *));
	void CreateBody();
	void UpdateBodyDimensionsArray();
	HRESULT UpdateCurrentBodiesArray() const;
	void ChangeCurrentCopyBody();
};

OBJECT_ENTRY_AUTO(__uuidof(UserPropertyManagerPage), CUserPropertyManagerPage)
