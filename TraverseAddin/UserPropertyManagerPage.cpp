// UserPropertyManagerPage.cpp : Implementation of CUserPropertyManagerPage

#include "stdafx.h"
#include "UserPropertyManagerPage.h"
#include "PMPageHandler.h"
#include "TraverseAddin.h"
#include <string>
#include <algorithm>

// CUserPropertyManagerPage

void CUserPropertyManagerPage::Init(CTraverseAddin *app)
{
	UserAddIn = app;
	ISwApp = UserAddIn->GetSldWorksPtr();
	CreatePropertyManagerPage();
	BodyArrNames = CSimpleArray<BSTR>();
}

//Create the handler and property page, and add the controls
void CUserPropertyManagerPage::CreatePropertyManagerPage()
{
	CComObject<CPMPageHandler>::CreateInstance(&Handler);
	Handler->AddRef();
	// pass add-in object
	Handler->Init(UserAddIn, this);
	CComBSTR title;
	title.LoadString(IDS_PMP_TITLE);
	long errors = 0;
	const long options = swPropertyManagerOptions_OkayButton | swPropertyManagerOptions_CancelButton;
	ISwApp->ICreatePropertyManagerPage(title, options, Handler, &errors, &SwPropertyPage);
	if (SwPropertyPage != nullptr)
		AddControls();
}

void CUserPropertyManagerPage::Destroy()
{
	SwPropertyPage->Close(VARIANT_FALSE); //same as pressing cancel
	SwPropertyPage = nullptr;

	Handler->Release();
	Handler = nullptr;
}

//Display the property page
void CUserPropertyManagerPage::Show() const
{
	long rVal;
	SwPropertyPage->Show(&rVal);
}

//Adds controls
void CUserPropertyManagerPage::AddControls()
{
	CComBSTR caption;
	const long options = -1;
	CComBSTR tip;

	caption.LoadString(IDS_PMP_LIST1_CAPTION);
	tip.LoadString(IDS_PMP_LIST1_TIP);
	SwPropertyPage->IAddControl(LIST_PROPERTIES, swControlType_Listbox, caption, swControlAlign_LeftEdge, options, tip, &Control);
	Control->QueryInterface(__uuidof(IPropertyManagerPageListbox), reinterpret_cast<void**>(&ListProperties));
	if (ListProperties != nullptr)
	{
		ListProperties->put_Height(120);
	}

	caption.LoadString(IDS_PMP_CHECKBOX_BODY_COPY_CAPTION);
	tip.LoadString(IDS_PMP_CHECKBOX_BODY_COPY_TIP);
	SwPropertyPage->IAddControl(CHECKBOX_BODY_COPY, swControlType_Checkbox, caption, swControlAlign_LeftEdge, options, tip, &Control);
	Control->QueryInterface(__uuidof(IPropertyManagerPageCheckbox), reinterpret_cast<void**>(&CheckboxBodyCopy));
	if (CheckboxBodyCopy != nullptr)
	{
		CheckboxBodyCopy->put_Checked(DEFAULT_BODY_COPY);
	}

	caption.LoadString(IDS_PMP_COMBOBOX_BODY_COPY_CAPTION);
	tip.LoadString(IDS_PMP_COMBOBOX_BODY_COPY_TIP);
	SwPropertyPage->IAddControl(COMBOBOX_BODY_COPY, swControlType_Combobox, caption, swControlAlign_LeftEdge, options, tip, &Control);
	Control->QueryInterface(__uuidof(IPropertyManagerPageCombobox), reinterpret_cast<void**>(&ComboboxBodyCopy));

	caption.LoadString(IDS_PMP_COMBOBOX1_CAPTION);
	tip.LoadString(IDS_PMP_COMBOBOX1_TIP);
	SwPropertyPage->IAddControl(COMBOBOX_BODY_TYPE, swControlType_Combobox, caption, swControlAlign_LeftEdge, options, tip, &Control);
	Control->QueryInterface(__uuidof(IPropertyManagerPageCombobox), reinterpret_cast<void**>(&ComboboxBodyType));
	if (ComboboxBodyType != nullptr)
	{
		_TCHAR* items[3];
		items[BOX_ITEM] = _T("Box");
		items[CYLINDER_ITEM] = _T("Cylinder");
		//items[CONE_ITEM] = _T("Cone");
		ComboboxBodyType->IAddItems(2, static_cast<BSTR*>(items));
	}

	AddNumberBoxes({
		{ &NumCenterX, IDS_PMP_NUM_CENTER_X_CAPTION, IDS_PMP_NUM_CENTER_X_TIP, NUM_CENTER_X },
		{ &NumCenterY, IDS_PMP_NUM_CENTER_Y_CAPTION, IDS_PMP_NUM_CENTER_Y_TIP, NUM_CENTER_Y },
		{ &NumCenterZ, IDS_PMP_NUM_CENTER_Z_CAPTION, IDS_PMP_NUM_CENTER_Z_TIP, NUM_CENTER_Z },
		{ &NumAxisX, IDS_PMP_NUM_AXIS_X_CAPTION, IDS_PMP_NUM_AXIS_X_TIP, NUM_AXIS_X },
		{ &NumAxisY, IDS_PMP_NUM_AXIS_Y_CAPTION, IDS_PMP_NUM_AXIS_Y_TIP, NUM_AXIS_Y },
		{ &NumAxisZ, IDS_PMP_NUM_AXIS_Z_CAPTION, IDS_PMP_NUM_AXIS_Z_TIP, NUM_AXIS_Z },
		{ &NumWidth, IDS_PMP_NUM_WIDTH_CAPTION, IDS_PMP_NUM_WIDTH_TIP, NUM_WIDTH },
		{ &NumLength, IDS_PMP_NUM_LENGTH_CAPTION, IDS_PMP_NUM_LENGTH_TIP, NUM_LENGTH },
		{ &NumHeight, IDS_PMP_NUM_HEIGHT_CAPTION, IDS_PMP_NUM_HEIGHT_TIP, NUM_HEIGHT },
		{ &NumRadius, IDS_PMP_NUM_RADIUS_CAPTION, IDS_PMP_NUM_RADIUS_TIP, NUM_RADIUS },
		{ &NumBaseRadius, IDS_PMP_NUM_BASE_RADIUS_CAPTION, IDS_PMP_NUM_BASE_RADIUS_TIP, NUM_BASE_RADIUS },
		{ &NumTopRadius, IDS_PMP_NUM_TOP_RADIUS_CAPTION, IDS_PMP_NUM_TOP_RADIUS_TIP, NUM_TOP_RADIUS }
	});
	// Activate number boxes for certain type of body
	ActivateNewBodyControls(DEFAULT_BODY_TYPE);
}

void CUserPropertyManagerPage::AddNumberBoxes(std::initializer_list<CUserPropertyManagerPage::ControlConfig<IPropertyManagerPageNumberbox>> params)
{
	CComBSTR caption;
	long options = ~swControlOptions_Visible;
	CComBSTR tip;

	std::for_each(params.begin(), params.end(), [&caption, &tip, options, this](const ControlConfig<IPropertyManagerPageNumberbox> &config) mutable {
		caption.LoadString(config.Caption);
		tip.LoadString(config.Tip);
		SwPropertyPage->IAddControl(config.Id, swControlType_Numberbox, caption, swControlAlign_LeftEdge, options, tip, &Control);
		Control->QueryInterface(__uuidof(IPropertyManagerPageNumberbox), reinterpret_cast<void**>(config.Pointer));
		(*config.Pointer)->SetRange(swNumberBox_Length, 0, 100, 0.001, VARIANT_TRUE);
		(*config.Pointer)->put_Value(0.001);
	});
}

void CUserPropertyManagerPage::ActivateNewBodyControls(const long activationBodyId)
{
	// deactivate copy body controls
	ApplyControlMethod({
		ComboboxBodyCopy
	}, [](IPropertyManagerPageControl* control) {control->put_Visible(VARIANT_FALSE); });

	// activate common new body controls
	ApplyControlMethod({
		ComboboxBodyType,
		NumCenterX,
		NumCenterY,
		NumCenterZ,
		NumAxisX,
		NumAxisY,
		NumAxisZ,
		NumHeight
	}, [](IPropertyManagerPageControl* control) {control->put_Visible(VARIANT_TRUE); });

	// activate non-common new body controls
	switch (activationBodyId)
	{
	case BOX_ITEM:
		ApplyControlMethod({
			NumWidth,
			NumLength,
		}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_TRUE); });

		ApplyControlMethod({
			NumRadius,
			NumBaseRadius,
			NumTopRadius
		}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_FALSE); });

		break;
	case CONE_ITEM:
		ApplyControlMethod({
			NumBaseRadius,
			NumTopRadius,
		}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_TRUE); });

		ApplyControlMethod({
			NumWidth,
			NumLength,
			NumRadius
		}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_FALSE); });
		break;
	case CYLINDER_ITEM:
		ApplyControlMethod({
			NumRadius,
		}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_TRUE); });

		ApplyControlMethod({
			NumWidth,
			NumLength,
			NumBaseRadius,
			NumTopRadius
		}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_FALSE); });
		break;
	default:
		break;
	}

	// make combobox visible if it isn't
	ApplyControlMethod({ ComboboxBodyType }, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_TRUE); });
}

void CUserPropertyManagerPage::ActivateCopyBodyControls()
{
	// deactivate new body controls
	ApplyControlMethod({
		ComboboxBodyType,
		NumCenterX,
		NumCenterY,
		NumCenterZ,
		NumAxisX,
		NumAxisY,
		NumAxisZ,
		NumWidth,
		NumLength,
		NumHeight,
		NumRadius,
		NumBaseRadius,
		NumTopRadius
	}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_FALSE); });

	// activate copy body controls
	ApplyControlMethod({
		ComboboxBodyCopy
	}, [](IPropertyManagerPageControl * control) {control->put_Visible(VARIANT_TRUE); });

	// add bodies names to comboboxBodyCopy
	CComPtr <IModelDoc2> pModel;
	CComQIPtr <IPartDoc> pPart;
	VARIANT vBodyArr;

	ISwApp->get_IActiveDoc2(&pModel);
	pModel->ClearSelection2(VARIANT_TRUE);
	pModel->QueryInterface(__uuidof(IPartDoc), reinterpret_cast<void**>(&pPart));
	const auto hr = pPart->GetBodies2(swSolidBody, VARIANT_TRUE, &vBodyArr);

	if (SUCCEEDED(hr) && (nullptr != vBodyArr.parray)) {
		BodyArrNames.RemoveAll();
		ComboboxBodyCopy->Clear();
		ArrayProcess<IBody2>(&vBodyArr, &CUserPropertyManagerPage::BodyNameAdd);
	}
}

void CUserPropertyManagerPage::ApplyControlMethod(std::initializer_list<IDispatch*> controls, void(*method)(IPropertyManagerPageControl *))
{
	std::for_each(controls.begin(), controls.end(), [method, this](IDispatch* controlChild) {
		controlChild->QueryInterface(__uuidof(IPropertyManagerPageControl), reinterpret_cast<void**>(&Control));
		method(Control);
		Control->Release();
	});
}

void CUserPropertyManagerPage::CreateBody()
{
	CComPtr <IModelDoc2> pModel;
	CComQIPtr <IPartDoc> pPart;
	CComPtr <IBody2> newBody;
	CComPtr <IFeature> feature;
	VARIANT_BOOL copyChecked;

	// We must init CComPtr<IModeler> to get address of it below
	// TODO: create constructor to init all this smart ptr?
	Modeler = nullptr;

	// get part doc
	ISwApp->get_IActiveDoc2(&pModel);
	pModel->ClearSelection2(VARIANT_TRUE);
	pModel->QueryInterface(__uuidof(IPartDoc), reinterpret_cast<void**>(&pPart));

	CheckboxBodyCopy->get_Checked(&copyChecked);
	if (copyChecked)
	{
		if (CurrentCopyBody != nullptr) {
			CurrentCopyBody->ICopy(&newBody);
		}
	}
	else
	{
		ComboboxBodyType->get_CurrentSelection(&CurrentSelection);
		ISwApp->IGetModeler(&Modeler);
		UpdateBodyDimensionsArray();

		switch (CurrentSelection)
		{
		case BOX_ITEM:
			Modeler->ICreateBodyFromBox2(BodyDimArray, &newBody);
			break;
		case CYLINDER_ITEM:
			Modeler->ICreateBodyFromCyl2(BodyDimArray, &newBody);
			break;
		case CONE_ITEM:
			Modeler->ICreateBodyFromCone2(BodyDimArray, &newBody);
			break;
		default:
			break;
		}

		//pPart->ICreateFeatureFromBody4(newBody, VARIANT_FALSE, swCreateFeatureBodyCheck + swCreateFeatureBodySimplify, &feature);
	}
	pPart->ICreateFeatureFromBody4(newBody, VARIANT_FALSE, swCreateFeatureBodyCheck + swCreateFeatureBodySimplify, &feature);
}

void CUserPropertyManagerPage::UpdateBodyDimensionsArray()
{
	ComboboxBodyType->get_CurrentSelection(&CurrentSelection);

	NumCenterX->get_Value(&BodyDimArray[0]);
	NumCenterY->get_Value(&BodyDimArray[1]);
	NumCenterZ->get_Value(&BodyDimArray[2]);
	NumAxisX->get_Value(&BodyDimArray[3]);
	NumAxisY->get_Value(&BodyDimArray[4]);
	NumAxisZ->get_Value(&BodyDimArray[5]);

	switch (CurrentSelection)
	{
	case BOX_ITEM:
		NumWidth->get_Value(&BodyDimArray[6]);
		NumLength->get_Value(&BodyDimArray[7]);
		NumHeight->get_Value(&BodyDimArray[8]);
		break;
	case CYLINDER_ITEM:
		NumRadius->get_Value(&BodyDimArray[6]);
		NumHeight->get_Value(&BodyDimArray[7]);
		break;
	case CONE_ITEM:
		NumBaseRadius->get_Value(&BodyDimArray[6]);
		NumTopRadius->get_Value(&BodyDimArray[7]);
		NumHeight->get_Value(&BodyDimArray[8]);
		break;
	default:
		break;
	}
}

HRESULT CUserPropertyManagerPage::UpdateCurrentBodiesArray() const
{
	CComPtr <IModelDoc2> pModel;
	CComQIPtr <IPartDoc> pPart;
	VARIANT vBodyArr;

	ISwApp->get_IActiveDoc2(&pModel);
	pModel->ClearSelection2(VARIANT_TRUE);
	pModel->QueryInterface(__uuidof(IPartDoc), reinterpret_cast<void**>(&pPart));
	const auto hr = pPart->GetBodies2(swSolidBody, VARIANT_TRUE, &vBodyArr);
	return hr;
}

void CUserPropertyManagerPage::ChangeCurrentCopyBody()
{
	CComPtr <IModelDoc2> pModel;
	CComQIPtr <IPartDoc> pPart;
	VARIANT vBodyArr;
	short currentSelection = 0;
	CurrentCopyBodyName = SysAllocString(L"");

	ISwApp->get_IActiveDoc2(&pModel);
	pModel->ClearSelection2(VARIANT_TRUE);
	pModel->QueryInterface(__uuidof(IPartDoc), reinterpret_cast<void**>(&pPart));
	const auto hr = pPart->GetBodies2(swSolidBody, VARIANT_TRUE, &vBodyArr);
	ComboboxBodyCopy->get_CurrentSelection(&currentSelection);
	ComboboxBodyCopy->get_ItemText(currentSelection, &CurrentCopyBodyName);

	// set current body for copy
	if (SUCCEEDED(hr) && (nullptr != vBodyArr.parray)) {
		ArrayProcess<IBody2>(&vBodyArr, &CUserPropertyManagerPage::SetBodyForCopy);
	}
}

void CUserPropertyManagerPage::DisplayProperties()
{
	CComPtr <IModelDoc2> pModel;
	CComQIPtr <IPartDoc> pPart;
	VARIANT vBodyArr;

	ISwApp->get_IActiveDoc2(&pModel);
	pModel->ClearSelection2(VARIANT_TRUE);
	pModel->QueryInterface(__uuidof(IPartDoc), reinterpret_cast<void**>(&pPart));
	const auto hr = pPart->GetBodies2(swSolidBody, VARIANT_TRUE, &vBodyArr);

	ListProperties->Clear();

	// display properties of bodies
	if (SUCCEEDED(hr) && (nullptr != vBodyArr.parray)) {
		ArrayProcess<IBody2>(&vBodyArr, &CUserPropertyManagerPage::BodyDisplay);
	}
	return;
}

CComPtr<IPropertyManagerPage2> CUserPropertyManagerPage::GetPropertyPagePtr() const
{
	return SwPropertyPage;
}

HRESULT  CUserPropertyManagerPage::ArrayCount(SAFEARRAY* pSafeArray, long * nResult)
{
	auto hr = S_OK;
	long nHighIndex = -1;

	// made it to prevent from corrupted stack in case bad safe array (bug in #5)
	const auto pnHighIndex = &nHighIndex;

	// verify that it's a one-dimensional array
	if (SafeArrayGetDim(pSafeArray) == 1)
	{
		// Get index number of highest array element
		// The array range is from 0 to highIndex
		hr = SafeArrayGetUBound(pSafeArray, 1, pnHighIndex);
	}

	*nResult = nHighIndex + 1;
	return hr;
}

//TODO: see what c++ standard (because can't cast from function pointer to std::function<void(CComQIPtr<T>)>)

//process Items in array (apply to them function fItem)
//Item can be any type, that Solidworks can give in SAFEARRAY (IEdge, IFace, IBody ...)
template<typename Item>
void  CUserPropertyManagerPage::ArrayProcess(VARIANT* pvItemArr, void (CUserPropertyManagerPage::*fItem)(CComQIPtr<Item>))
{
	const auto psaItems = V_ARRAY(pvItemArr);
	LPDISPATCH* pItemDisplayArray = nullptr;

	long nItemCount = 0;
	const auto pnItemCount = &nItemCount;

	ArrayCount(psaItems, pnItemCount);
	SafeArrayAccessData(psaItems, reinterpret_cast<void **>(&pItemDisplayArray));

	//we can't call member function from another class via function pointer, so we will call directly
	//TODO: create funct. classes (functors) or smth that can solve it and use std::for_each
	//std::for_each(pItemDisplayArray, pItemDispArray + nItemCount, std::mem_fun(fItem));
	if (pItemDisplayArray)
	{
		auto first = pItemDisplayArray;
		const auto last = pItemDisplayArray + nItemCount;
		for (; first != last; ++first)
			(this->*fItem)(*first);
	}

	// Unaccess & destroy the component SafeArray
	SafeArrayUnaccessData(psaItems);
	SafeArrayDestroy(psaItems);
}


void  CUserPropertyManagerPage::EdgeDisplay(CComQIPtr<IEdge> pEdge)
{
	CComPtr<ICurve> pCurve;
	CComPtr<ICurveParamData> pCurveData;
	long nIdentity;
	double nLength = 0;
	double pCurveParams[CURVE_PARAMS_NUM];

	pEdge->IGetCurve(&pCurve);
	pEdge->IGetCurveParams2(pCurveParams);
	pCurve->Identity(&nIdentity);
	pCurve->GetLength3(pCurveParams[CURVE_FIRST_LENGTH_PARAM], pCurveParams[CURVE_SECOND_LENGTH_PARAM], &nLength);

	const auto s = std::to_wstring(nLength);
	CComBSTR countString;
	countString.Attach(SysAllocStringLen(s.c_str(), s.length()));
	ListProperties->IAddItems(1, &countString);
}

void CUserPropertyManagerPage::FaceDisplay(CComQIPtr<IFace> pFace)
{
	VARIANT vEdgeArr;
	long nEdgeArea;
	pFace->GetEdgeCount(&nEdgeArea);

	const auto s = std::to_wstring(nEdgeArea);
	CComBSTR countString;
	countString.Attach(SysAllocStringLen(s.c_str(), s.length()));
	ListProperties->IAddItems(1, &countString);

	pFace->GetEdges(&vEdgeArr);

	// display properties of edges
	ArrayProcess<IEdge>(&vEdgeArr, &CUserPropertyManagerPage::EdgeDisplay);
}

void CUserPropertyManagerPage::BodyDisplay(CComQIPtr<IBody2> pBody)
{
	CComBSTR sBodyName(L"");
	VARIANT vFaceArr;

	pBody->get_Name(&sBodyName);
	ListProperties->IAddItems(1, &sBodyName);

	pBody->GetFaces(&vFaceArr);

	// display properties of faces
	ArrayProcess<IFace>(&vFaceArr, &CUserPropertyManagerPage::FaceDisplay);
}

void CUserPropertyManagerPage::BodyNameAdd(CComQIPtr<IBody2> pBody)
{
	CComBSTR sBodyName(L"");
	pBody->get_Name(&sBodyName);
	BodyArrNames.Add(sBodyName);
	ComboboxBodyCopy->IAddItems(1, &sBodyName);
}

void CUserPropertyManagerPage::SetBodyForCopy(CComQIPtr<IBody2> pBody)
{
	auto sBodyName = SysAllocString(L"");
	pBody->get_Name(&sBodyName);
	if (wcscmp(sBodyName, CurrentCopyBodyName) == 0)
	{
		CurrentCopyBody = pBody;
	}
}
