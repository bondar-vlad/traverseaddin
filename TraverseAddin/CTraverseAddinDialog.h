#pragma once


// CTraverseAddinDialog dialog

class CTraverseAddinDialog : public CDialog
{
	DECLARE_DYNAMIC(CTraverseAddinDialog)

public:
	CTraverseAddinDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTraverseAddinDialog();

// Dialog Data
	enum { IDD = IDD_CTraverseAddinDialog };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
};
