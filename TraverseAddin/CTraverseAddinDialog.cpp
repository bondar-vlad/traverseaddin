// CTraverseAddinDialog.cpp : implementation file
//

#include "stdafx.h"


#include "CTraverseAddinDialog.h"


// CTraverseAddinDialog dialog

IMPLEMENT_DYNAMIC(CTraverseAddinDialog, CDialog)

CTraverseAddinDialog::CTraverseAddinDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CTraverseAddinDialog::IDD, pParent)
{

}

CTraverseAddinDialog::~CTraverseAddinDialog()
{
}

void CTraverseAddinDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTraverseAddinDialog, CDialog)
END_MESSAGE_MAP()


// CTraverseAddinDialog message handlers
