// PMPageHandler.cpp : Implementation of CPMPageHandler

#include "stdafx.h"
#include "PMPageHandler.h"
#include "TraverseAddin.h"

// CPMPageHandler

void CPMPageHandler::Init(CTraverseAddin* app, CUserPropertyManagerPage* pmp)
{
	travAddin = app;
	iSwApp = travAddin->GetSldWorksPtr();
	this->pmp = pmp;
}

STDMETHODIMP CPMPageHandler::OnClose(long Reason)
{
	switch (Reason)
	{
	case swPropertyManagerPageClose_Cancel:
		iSwApp->SendMsgToUser(L"You have canceled body creating");
		break;
	case swPropertyManagerPageClose_Okay:
		pmp->CreateBody();
		iSwApp->SendMsgToUser(L"Body was created");
		break;
	default:
		break;
	}
	return S_OK;
}

STDMETHODIMP CPMPageHandler::OnCheckboxCheck(long Id, VARIANT_BOOL Checked)
{
	switch (Id)
	{
	case CHECKBOX_BODY_COPY:
		if (Checked)
			pmp->ActivateCopyBodyControls();
		else
			pmp->ActivateNewBodyControls(DEFAULT_BODY_TYPE);
		break;
	default:
		break;
	}
	return S_OK;
}

STDMETHODIMP CPMPageHandler::OnComboboxSelectionChanged(long Id, long Item)
{
	switch (Id)
	{
	case COMBOBOX_BODY_TYPE:
		pmp->ActivateNewBodyControls(Item);
		break;
	case COMBOBOX_BODY_COPY:
		pmp->ChangeCurrentCopyBody();
		break;
	default:
		break;
	}
	return S_OK;
}

STDMETHODIMP CPMPageHandler::AfterActivation()
{
	pmp->DisplayProperties();
	return S_OK;
}

